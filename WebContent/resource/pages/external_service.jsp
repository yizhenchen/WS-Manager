<?xml version="1.0" encoding="UTF-8" ?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>ITM - Web Services Management</title>
        <meta name="description" content="AppUI is a Web App Bootstrap Admin Template created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

    </head>
    <body>
                        <!-- First Row -->
                        <div class="row">
                            <!-- Simple Stats Widgets -->
                            <div class="col-sm-6 col-lg-3">
                                <a href="javascript:void(0)" class="widget">
                                    <div class="widget-content widget-content-mini text-right clearfix">
                                        <div class="widget-icon pull-left themed-background">
                                            <i class="gi gi-cardio text-light-op"></i>
                                        </div>
                                        <h2 class="widget-heading h3">
                                            <strong><span data-toggle="counter" data-to="15"></span></strong>
                                        </h2>
                                        <span class="text-muted">Active </span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <a href="javascript:void(0)" class="widget">
                                    <div class="widget-content widget-content-mini text-right clearfix">
                                        <div class="widget-icon pull-left themed-background-success">
                                            <i class="gi gi-cardio text-light-op"></i>
                                        </div>

                                        <h2 class="widget-heading h3 text-success">
                                            <strong><span data-toggle="counter" data-to="5"></span></strong>
                                        </h2>
                                        <span class="text-muted">Disabled</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <a href="javascript:void(0)" class="widget">
                                    <div class="widget-content widget-content-mini text-right clearfix">
                                        <div class="widget-icon pull-left themed-background-warning">
                                           <i class="gi gi-cardio text-light-op"></i>
                                        </div>
                                        <h2 class="widget-heading h3 text-warning">
                                            <strong> <span data-toggle="counter" data-to="10"></span></strong>
                                        </h2>
                                        <span class="text-muted">pending</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <a href="javascript:void(0)" class="widget">
                                    <div class="widget-content widget-content-mini text-right clearfix">
                                        <div class="widget-icon pull-left themed-background-danger">
                                           <i class="gi gi-cardio text-light-op"></i>
                                        </div>
                                        <h2 class="widget-heading h3 text-danger">
                                            <strong> <span data-toggle="counter" data-to="0"></span></strong>
                                        </h2>
                                        <span class="text-muted">EARNINGS</span>
                                    </div>
                                </a>
                            </div>
                            <!-- END Simple Stats Widgets -->
                        </div>
                        <!-- END First Row -->



 <div class="block full">   
                            <div class="block-title">
                                <h2>External Services</h2>

                                <div class=" pull-right" style="padding-right:21px;">
                                        <button type="button" class="btn  btn-danger" onClick="showModal(-1)">new a service</button>
                                </div>

                            </div>
                            <div class="table-responsive">
                                <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Service Name</th>
                                           <th >Router</th>
                                            <!-- <th style="max-width: 200px;">SQL</th> --> 
                                              <th >Domain&Path</th>
                                               
                                                  <th >Method</th>
                                            <th >Status</th>
                                            <th class="text-center" style="width: 200px;">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="services-list">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

        <!-- Example Package Modal -->
        <div id="modal-package" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                          <h1 class="h2" id="modal-title" ></h1>
                    </div>
                    <div class="modal-body">
                    	 <div class="row push">
                    	 	<div  class="col-sm-10" > 
                    			 <div class="row push">
                                    	<label class="col-sm-3 control-label" for="example-input-normal">Service Name :</label>
                                        <div class="col-sm-6">
                                         	<input type="text" id="input-service-name"  class="form-control" placeholder="Normal">
                                         </div>
                                  </div>
                                  
                               
                              <div class="row push">
                                    <label class="col-sm-3 control-label" for="example-input-normal">Method</label>
                                        <div class="col-sm-6">
                                    <select id="select-method" name="example-select" class="form-control" size="1">
                                                    <option value="0">GET</option>
                                                    <option value="1">POST</option>
                                                </select>
                                        </div>
                               </div>     
                                <div class="row push">
                                    <label class="col-sm-3 control-label" for="example-input-normal">Router</label>
                                        <div class="col-sm-6">
                                         <input type="text" id="input-router"  class="form-control" placeholder="Normal">
                                         </div>
                                    </div>
                                <div class="row push">
                                    <label class="col-sm-3 control-label" for="example-input-normal">Domain</label>
                                        <div class="col-sm-6">
                                         <input type="text" id="input-domain"  class="form-control" placeholder="Normal">
                                         </div>
                                    </div>   
                                <div class="row push">
                                    <label class="col-sm-3 control-label" for="example-input-normal">Path</label>
                                        <div class="col-sm-6">
                                        <input type="text" id="input-path"  class="form-control" placeholder="Normal">
                                        </div>
                               </div>  
                                <div class="row push">
                                    <label class="col-sm-3 control-label" for="example-input-normal">Status</label>
                                        <div class="col-sm-6">
                                          <select id="select-status" name="example-select" class="form-control" size="1">
                                                    <option value="0">Active</option>
                                                    <option value="1">Pending</option>
                                                     <option value="2">Warning</option>
                                                      <option value="3">Disabled</option>
                                                </select>
                                        </div>
                               </div>
                         
                                <div class="row push">
                                    <label class="col-sm-3 control-label" for="example-input-normal">Description</label>
                                        <div class="col-sm-6">
                                        <textarea id="textarea-desc" name="example-textarea-input" rows="7" class="form-control" placeholder="Description.."></textarea>
                                        </div>
                               </div>  
                                <button type="button" id="btn-services-update" class="btn btn-effect-ripple btn-primary">Save</button>
                                 <h1 class="h2"><strong>Test</strong> </h1>
                                 <br>
                                <div class="row push">
                                    <label class="col-sm-3 control-label" for="example-input-normal">url</label>
                                        <div class="col-sm-6">
                                        <input type="text" id="input-url"  class="form-control" placeholder="Normal">
                                        </div>
                               </div>  
                                 <div class="row push">
                                    <label class="col-sm-3 control-label" for="example-input-normal">Method</label>
                                        <div class="col-sm-6">
                                    <select id="select-method2" name="example-select" class="form-control" size="1">
                                                    <option value="0">GET</option>
                                                    <option value="1">POST</option>
                                                </select>
                                        </div>
                               </div>     
                               <div class="row push">
                                    <label class="col-sm-3 control-label" for="example-input-normal">Data</label>
                                        <div class="col-sm-6">
                                        <textarea id="textarea-data" name="example-textarea-input" rows="7" class="form-control" placeholder="Data"></textarea>
                                        </div>
                               </div>  
                               
                               
                              <div class="row push">
                                    <label class="col-sm-3 control-label" for="example-input-normal">Result</label>
                                        <div class="col-sm-6" >
                                        <textarea readonly id="result" name="example-textarea-input" rows="7" class="form-control" placeholder="Data"></textarea>
                                        </div>
                               </div>  
                               
                               
                               
                                <button  id="btn-services-test" type="button" class="btn btn-effect-ripple btn-primary">Test</button>
                               
                   		 </div>
                   		 </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-2.1.1.min.js"%3E%3C/script%3E'));</script>

       <script src="<c:url value="/resource/pages/js/notify.js"/>"></script>
        <script>
      
       
     
        // ------init the panel : get all the services ----//
        
        var token = getParameterByName("token")
      	function init(){
				
              $.ajax({
                url :"../../"+token+"/itm3/services/external",
                type: "Get",
                contentType:"application/json; charset=utf-8",
                success: function(data, textStatus, jqXHR)
                {
                    services = JSON.parse(data.obj)
                    if(data.code=="success"){
                    
                         initTable(services);
                         initStatusPanel(services);
                    }else{
                        $.notify(data.desc, 'warn');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    console.log(textStatus);
                }
            });
      }
      init();

      // ------------   end init -------------------//

      
      
      // --- used in the the init function :  init services table  -- //
      
      function initTable(obj){
        var html = "" ;
       for (var i = 0, len = obj.length; i < len; i++) {
              
                html +=  "<tr><td class=\"text-center\" ><strong>"+obj[i].name+"</strong>("+obj[i].id+")</td>";
                  html +=  "<td>"+obj[i].requested_router+"</td>";
             

                html +=  "<td>"+obj[i].domain+"&"+obj[i].path+"</td>";


                    html +=  "<td>"+obj[i].method+"</td>";

                switch(obj[i].status) {
                    case "Pending":
                         html += "<td><span class=\"label label-warning\">Pending..</span></td>";
                        break;
                    case "Active":
                         html +="<td><span class=\"label label-success\">Active..</span></td>";
                        break;
                    case "Warning":
                         html +="<td><span class=\"label label-danger\">Warning..</span></td>";
                        break;
                     case "Disable":
                         html +="<td><span class=\"label label-default\">Disable..</span></td>";
                        break;
                    default:
                        html += "<td></td>";
                }
               
             
                 html += "<td class=\"text-center\"> ";   
                 html += " <button data-uuid="+obj[i].id+"  type=\"button\" class=\"btn btn-primary\" onClick=\"Del("+obj[i].id+") \"> Del</button>";
                 html += " <button data-uuid="+obj[i].id+"  type=\"button\" class=\"btn btn-primary\" onClick=\"showModal("+i+") \"> Edit</button>";
                 if(obj[i].status != "Active") {
                 	html += " <button data-uuid="+obj[i].id+"  type=\"button\" class=\"btn btn-primary\" onClick=\"active("+obj[i].id+") \">Activate</button>";
                 }else{
                	 html += " <button data-uuid="+obj[i].id+"  type=\"button\" class=\"btn btn-primary\" onClick=\"disable("+obj[i].id+") \">Disable</button>";
                 }
                 html += "</td> </tr>";
         }

         $("#services-list").html(html);

      }
      // ------------   end init table -------------------//

      
   
      
      function initStatusPanel(obj){

			// to be implement 
      }


      // --- init page setting , current only init the  notation javascript --- // 

     function  pageInit(){
      $.notify('Check Something ', 'info');
     }

      
      
      
      
    function refresh(message,type){
    	//location.reload();
    	Clear();
    	init();
    	$.notify(message,type);
    }
    
    var services;
    
    
    
    function Clear(){
    	$("#services-list").html("");
    }


    
    //-- delete service , consider should be rmove , --//
    function Del(id){
    		var d = {
    				"database":"itm3",
    				"att":id,
    				"token":token
    		}
    		$.ajax({
                url : "../../services/del",
                type: "POST",
                data:JSON.stringify(d),
                contentType:"application/json; charset=utf-8",
                success: function(data, textStatus, jqXHR)
                {
                	refresh("delete operation done","success");
                    init();
                	
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                	refresh("delete operation fails ","warn");
                    //$("#result").text(jqXHR.responseText);
                }
            });
    	}

    // active service
        function active(obj){
              var jsondata = {
            "database":"itm3",
            "status":"",
            "id": obj,
            "token":token
            }
                 $.ajax({
                url :  "../../services/active",
                type: "POST",
                data:JSON.stringify(jsondata),
                contentType:"application/json; charset=utf-8",
                success: function(data, textStatus, jqXHR)
                {
                      if(data.code=="failure"){
                            $.notify(data.desc,"error");
                    }else{
                        $.notify(data.desc,"success");
                        init();
                    }   
                   $("#result").html(data.obj);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $("#result").text(jqXHR.responseText);
                }

            });
        	
        }

    // disable service
        function disable(obj){
             var jsondata = {
            "database":"itm3",
            "status":"",
            "id": obj,
            "token":token
          }

                 $.ajax({
                url :  "../../services/disable",
                type: "POST",
                data:JSON.stringify(jsondata),
                contentType:"application/json; charset=utf-8",
                success: function(data, textStatus, jqXHR)
                {
                      if(data.code=="failure"){
                            $.notify(data.desc,"error");
                    }else{
                        $.notify(data.desc,"success");
                        init();
                    }
                   $("#result").html(data.obj);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $("#result").text(jqXHR.responseText);
                }

            });
        	
        }
        
        
        function clearModal(){
           $("#input-router").val("");
           $("#input-query").val("");
           $("#textarea-desc").val("");
        }
        
    	function showModal(index){
    		

    		 if(index == -1){
    			 // calling a new services
    			  $("#modal-title").html("<strong>New Service</strong>");
            	  $("#modal-title").data("data-id",-1);
    			  $("#btn-services-update").data("data-methon","new");
                  clearModal();
    			 
    		 }else{
           $("#modal-title").data("data-id",services[index].id);
         	 $("#btn-services-update").data("data-methon","update");
    		 $("#modal-title").html("<strong>"+services[index].name+"</strong> <small> status : "+services[index].status+"</small>");
           $("#input-domain").val(services[index].domain);
            $("#input-router").val(services[index].requested_router);
           $("#input-path").val(services[index].path);
           $("#input-service-name").val(services[index].name);
           $("#textarea-desc").val(services[index].description);
           $('#select-method option:contains(' + services[index].method + ')').prop({selected: true});
           $('#select-status option:contains(' + services[index].status + ')').prop({selected: true});

    		 }

    	     $('#modal-package').modal({
    	        show: true
    	    });
    	}


      $("#btn-services-test").click(function(){

           if($( "#select-method2 option:selected" ).text()=="GET"){
             $.ajax({
                url : "../../"+token+"/"+$("#input-url").val(),
                type: "Get",
                contentType:"application/json; charset=utf-8",
                success: function(data, textStatus, jqXHR)
                {
                     if(data.code=="failure"){
                            $.notify(data.desc,"error");
                    }else{
                        $.notify(data.desc,"success");
                        init();
                    }
                  $("#result").html(data.obj);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                   
                    $("#result").text(jqXHR.responseText);
                }
            });
           }else{

             $.ajax({
                url :  "../.."+$("#input-url").val(),
                type: "POST",
                data:JSON.stringify(JSON.parse($("#textarea-data").val())),
                contentType:"application/json; charset=utf-8",
                success: function(data, textStatus, jqXHR)
                {
                      if(data.code=="failure"){
                            $.notify(data.desc,"error");
                    }else{
                        $.notify(data.desc,"success");
                        init();
                    }
                	 $("#result").html(data.obj);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                	  $("#result").text(jqXHR.responseText);
                }

           	});
           }
          
      });



      $("#btn-services-update").click(function(){
			   
           var jsondata = {
            "database":"itm3",
            "domain":$("#input-domain").val(),
             "router":$("#input-router").val(),
            "method":$( "#select-method option:selected" ).text(),
            "path":$("#input-path").val().replace(/'/g,"\\'"),
            "status":$( "#select-status option:selected" ).text(),
            "time": new Date(),
            "editor":localStorage.username,
            "name":$("#input-service-name").val(),
            "desc":$("#textarea-desc").val(),
            "type":"External",
            "id": $("#modal-title").data("data-id"),
            "token":token
          }

         if($("#btn-services-update").data("data-methon")=="new"){
                $.ajax({
                url :  "../../services/create",
                type: "POST",
                data:JSON.stringify(jsondata),
                contentType:"application/json; charset=utf-8",
                success: function(data, textStatus, jqXHR)
                {
                      if(data.code=="failure"){
                            $.notify(data.desc,"error");
                    }else{
                        $.notify(data.desc,"success");
                        init();
                    }
                   $("#result").html(data.obj);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $("#result").text(jqXHR.responseText);
                }

            });

          }else{
               $.ajax({
                url :  "../../services/update",
                type: "POST",
                data:JSON.stringify(jsondata),
                contentType:"application/json; charset=utf-8",
                success: function(data, textStatus, jqXHR)
                {
                      if(data.code=="failure"){
                            $.notify(data.desc,"error");
                    }else{
                        $.notify(data.desc,"success");
                        init();
                    }
                   $("#result").html(data.obj);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $("#result").text(jqXHR.responseText);
                }

            });
        

        }



      });

      function getParameterByName(name, url) {
    	    if (!url) url = window.location.href;
    	    name = name.replace(/[\[\]]/g, "\\$&");
    	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    	        results = regex.exec(url);
    	    if (!results) return null;
    	    if (!results[2]) return '';
    	    return decodeURIComponent(results[2].replace(/\+/g, " "));
    	}

        </script>
    </body>
</html>
