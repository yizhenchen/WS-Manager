<?xml version="1.0" encoding="UTF-8" ?>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>ITM- Web Services Management</title>

        <meta name="description" content="AppUI is a Web App Bootstrap Admin Template created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

  <link rel="shortcut icon" href="img/favicon.png">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="<c:url value="/resource/css/bootstrap.min.css"/>">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="<c:url value="/resource/css/plugins.css"/>">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="<c:url value="/resource/css/main.css"/>">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="<c:url value="/resource/css/themes/social.css"/>">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) -->
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
  <!-- Login Container -->
        <div id="login-container">
            <!-- Login Header -->
            <h1 class="h2 text-light text-center push-top-bottom animation-slideDown" style="color:white;">
               <strong>Web Services Management</strong>
            </h1>
            <!-- END Login Header -->

            <!-- Login Block -->
           <div id="ohsnap"></div>
            <div class="block animation-fadeInQuickInv">
                <!-- Login Title -->
                <div class="block-title">
                 
                    <h2>Please Login</h2>
                </div>
                <!-- END Login Title -->
 				
                <!-- Login Form -->
                <form id="form-login" action="index.html" method="post" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input type="text" id="login-email" name="login-email" class="form-control" placeholder="Your email..">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input type="password" id="login-password" name="login-password" class="form-control" placeholder="Your password..">
                        </div>
                    </div>
                    <div class="form-group form-actions">
                 	 <div class="col-xs-8">
                        
                        </div> 
                        <div class="col-xs-4 text-right">
                           <!--  <button type="submit" class="btn btn-effect-ripple btn-sm btn-primary"><i class="fa fa-check"></i> Let's Go</button> -->
                            <a type="submit"  id="btn-enter" class="btn btn-effect-ripple btn-sm btn-primary" ><i class="fa fa-check"></i>&nbsp;Enter</a>
                        </div>
                    </div>
                </form>
              
                <!-- END Login Form -->
            </div>
            <!-- END Login Block -->
              
        </div>
        <!-- END Login Container -->
        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-2.1.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="<c:url value="/resource/js/vendor/bootstrap.min.js"/>"></script>
        <script src="<c:url value="/resource/js/plugins.js"/>"></script>
        <script src="<c:url value="/resource/js/app.js"/>"></script>
          <script src="<c:url value="/resource/js/ohsnap.min.js"/>"></script>

        <!-- Load and execute javascript code used only in this page -->
        <SCRIPT type="text/javascript">
        
        $("#btn-enter").click(function(){
        	var info = { 
            		"database":"itm3",
            		"username":$("#login-email").val().trim(),
            		"pwd":$("#login-password").val().trim()
            		}
            $.ajax({
                url :"login",
                type: "POST",
                contentType:"application/json; charset=utf-8",
                data : info,
                success: function(data, textStatus, jqXHR)
                {
                	var o = JSON.parse(data.obj)
                	if(data.code=="success"){
                		$(location).attr("href","resource/pages/welcome.jsp?token="+o[0].token);
                	}else{
                		ohSnap('Wrong username or password, try again . ', {color: 'red','duration':'1500'});
                	}
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                	console.log(textStatus);
                }
            });
        })
        
        
        </SCRIPT>
        
    </body>
</html>