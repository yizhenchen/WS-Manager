<?xml version="1.0" encoding="UTF-8" ?>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>ITM- Web Services Management</title>

        <meta name="description" content="AppUI is a Web App Bootstrap Admin Template created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

    </head>
    <body>
       
                    <div id="page-content">


                        <!-- First Row -->
                        <div class="row">
                            <!-- Simple Stats Widgets -->
                            <div class="col-sm-6 col-lg-3">
                                <a href="javascript:void(0)" class="widget">
                                    <div class="widget-content widget-content-mini text-right clearfix">
                                        <div class="widget-icon pull-left themed-background">
                                            <i class="gi gi-cardio text-light-op"></i>
                                        </div>
                                        <h2 class="widget-heading h3">
                                            <strong><span data-toggle="counter" data-to="15"></span></strong>
                                        </h2>
                                        <span class="text-muted">Active </span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <a href="javascript:void(0)" class="widget">
                                    <div class="widget-content widget-content-mini text-right clearfix">
                                        <div class="widget-icon pull-left themed-background-success">
                                            <i class="gi gi-cardio text-light-op"></i>
                                        </div>

                                        <h2 class="widget-heading h3 text-success">
                                            <strong><span data-toggle="counter" data-to="5"></span></strong>
                                        </h2>
                                        <span class="text-muted">Disabled</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <a href="javascript:void(0)" class="widget">
                                    <div class="widget-content widget-content-mini text-right clearfix">
                                        <div class="widget-icon pull-left themed-background-warning">
                                           <i class="gi gi-cardio text-light-op"></i>
                                        </div>
                                        <h2 class="widget-heading h3 text-warning">
                                            <strong> <span data-toggle="counter" data-to="10"></span></strong>
                                        </h2>
                                        <span class="text-muted">pending</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-lg-3">
                                <a href="javascript:void(0)" class="widget">
                                    <div class="widget-content widget-content-mini text-right clearfix">
                                        <div class="widget-icon pull-left themed-background-danger">
                                           <i class="gi gi-cardio text-light-op"></i>
                                        </div>
                                        <h2 class="widget-heading h3 text-danger">
                                            <strong> <span data-toggle="counter" data-to="0"></span></strong>
                                        </h2>
                                        <span class="text-muted">EARNINGS</span>
                                    </div>
                                </a>
                            </div>
                            <!-- END Simple Stats Widgets -->
                        </div>
                        <!-- END First Row -->

                      



 <div class="block full">   
                            <div class="block-title">
                                <h2>Services</h2>
                            </div>
                            <div class="table-responsive">
                                <table id="example-datatable" class="table table-striped table-bordered table-vcenter">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width:200px;">Service Name</th>
                                            <th>Description</th>
                                             <th style="width: 120px;">Time</th>
                                            <th style="width: 120px;">Status</th>
                                            <th class="text-center" style="width: 250px;">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                         
                                            <td class="text-center" ><strong>AppUser30</strong></td>
                                            <td>app.user30@example.com</td>
                                            <td>1515:123:!23</td>
                                            <td><span class="label label-warning">Pending..</span></td>
                                            <td class="text-center">
                                              

                                             <!--  <button type="button" class="btn btn-primary">Disable</button> -->
                                              <a data-uuid="123" type="button" class="openBtn btn btn-primary">view</a>
                                              <button type="button" class="btn btn-primary">Edit</button>
                                              <button type="button" class="btn btn-primary">Activate</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>



                        
                    </div>
               



        <!-- Example Package Modal -->
        <div id="modal-package" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <ul class="nav nav-tabs" data-toggle="tabs">
                            <li class="active"><a href="#modal-tabs-package"><i class="gi gi-package"></i> Package</a></li>
                            <li><a href="#modal-tabs-reviews"><i class="fa fa-pencil"></i> Reviews</a></li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="modal-tabs-package">
                                <h1 class="h2"><strong>Italian Pizza Night</strong> <small>(2 People)</small></h1>
                                <div class="row push">
                                    <div class="col-xs-6">
                                        <img src="img/placeholders/photos/photo11.jpg" alt="Image" class="img-responsive">
                                    </div>
                                    <div class="col-xs-6">
                                        <img src="img/placeholders/photos/photo2.jpg" alt="Image" class="img-responsive">
                                    </div>
                                </div>
                                Mauris tincidunt tincidunt turpis in porta. Integer fermentum tincidunt auctor. Vestibulum ullamcorper, odio sed rhoncus imperdiet, enim elit sollicitudin orci, eget dictum leo mi nec lectus. Nam commodo turpis id lectus scelerisque vulputate. Integer sed dolor erat. Fusce erat ipsum, varius vel euismod sed, tristique et lectus? Etiam egestas fringilla enim, id convallis lectus laoreet at. Fusce purus nisi, gravida sed consectetur ut, interdum quis nisi. Quisque egestas nisl id lectus facilisis scelerisque? Proin rhoncus dui at ligula vestibulum ut facilisis ante sodales! Suspendisse potenti. Aliquam tincidunt sollicitudin sem nec ultrices. Sed at mi velit. Ut egestas tempor est, in cursus enim venenatis eget! Nulla quis ligula ipsum
                            </div>
                            <div class="tab-pane" id="modal-tabs-reviews">
                                <div class="text-center text-warning push">
                                    <i class="fa fa-2x fa-star"></i>
                                    <i class="fa fa-2x fa-star"></i>
                                    <i class="fa fa-2x fa-star"></i>
                                    <i class="fa fa-2x fa-star"></i>
                                    <i class="fa fa-2x fa-star"></i>
                                </div>
                                <div class="text-center">
                                    <strong>5.0</strong> /5.0 (3 Ratings)
                                </div>
                                <hr>
                                <ul class="media-list">
                                    <li class="media">
                                        <a href="javascript:void(0)" class="pull-left">
                                            <img src="img/placeholders/avatars/avatar12.jpg" alt="Avatar" class="img-circle">
                                        </a>
                                        <div class="media-body">
                                            <span class="text-warning">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                            <a href="javascript:void(0)"><strong>Nicoline Andersen</strong></a>
                                            <p class="push-bit"><span class="label label-success">Positive</span> Authentic Italian Experience, Friendly People, Delicious Pizzas</p>
                                            <p><span class="label label-danger">Negative</span> None!</p>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <a href="javascript:void(0)" class="pull-left">
                                            <img src="img/placeholders/avatars/avatar13.jpg" alt="Avatar" class="img-circle">
                                        </a>
                                        <div class="media-body">
                                            <span class="text-warning">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                            <a href="javascript:void(0)"><strong>Josefina Orozco</strong></a>
                                            <p class="push-bit"><span class="label label-success">Positive</span> Friendly People, Delicious Pizzas</p>
                                            <p><span class="label label-danger">Negative</span> None!</p>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <a href="javascript:void(0)" class="pull-left">
                                            <img src="img/placeholders/avatars/avatar7.jpg" alt="Avatar" class="img-circle">
                                        </a>
                                        <div class="media-body">
                                            <span class="text-warning">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                            <a href="javascript:void(0)"><strong>Donald Pierce</strong></a>
                                            <p class="push-bit"><span class="label label-success">Positive</span> Delicious Pizzas, Great Price</p>
                                            <p><span class="label label-danger">Negative</span> None!</p>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <h4 class="pull-left"><span class="text-muted">$</span> <strong class="text-primary">49</strong></h4>
                        <button type="button" class="btn btn-effect-ripple btn-success"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                        <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-2.1.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="<c:url value="/resource/js/vendor/bootstrap.min.js"/>"></script>
        <script src="<c:url value="/resource/js/plugins.js"/>"></script>
        <script src="<c:url value="/resource/js/app.js"/>"></script>

        <!-- Load and execute javascript code used only in this page -->
       
        <script>$(function(){

$('.openBtn').click(function () {
    console.log(this.getAttribute('data-uuid'));
    showModal()
});

function showModal(){
     $('#modal-package').modal({
        show: true
    });
}

            });</script>
    </body>
</html>
