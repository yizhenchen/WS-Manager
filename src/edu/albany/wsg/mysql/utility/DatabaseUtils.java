package edu.albany.wsg.mysql.utility;


import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

@Component
@Scope(value="session",proxyMode = ScopedProxyMode.TARGET_CLASS)
public class DatabaseUtils {
	
	final static Logger logger = Logger.getLogger(DatabaseUtils.class);
	
	 private DriverManagerDataSource dataSource ;
	 private JdbcTemplate jdbcTemplate;
	 private String databaseName;
	 public DatabaseUtils(){
		 this.dataSource =new DriverManagerDataSource();
		 this.dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		 this.dataSource.setUsername("root");
		 //this.dataSource.setPassword("DtZBAxrN57Mx");
		 this.dataSource.setPassword("123456");
		 this.jdbcTemplate =new JdbcTemplate();
		
	 }
	 
	public DriverManagerDataSource getDataSource() {
		return dataSource;
	}
	public void setURL(String db) {
		this.databaseName = db;
		this.dataSource.setUrl("jdbc:mysql://localhost:3306/"+db);
		this.jdbcTemplate.setDataSource(this.dataSource);
		
	}

	
	public String getDatabaseName() {
		return databaseName;
	}

	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	 public boolean CheckDatabase(String database){
		 if(null==database||database.equals("")){
			// return false;
			 logger.error("no database be provided.");
		 }else{
			
			 return true;
		 }
		 return false;
		 
	 }
}
