package edu.albany.wsg.mysql.utility;

import org.springframework.stereotype.Component;

@Component
public class ResponseUtil {

	 
	public CustomResponse databaseSettingFail(){
			CustomResponse response = new CustomResponse();
			response.setCode(ResponseStatus.failure);
			response.setDesc("empty database");
			return response;
		}
	 
	 
		public CustomResponse fail(){
				CustomResponse response = new CustomResponse();
				response.setCode(ResponseStatus.failure);
				return response;
			}
	
}
