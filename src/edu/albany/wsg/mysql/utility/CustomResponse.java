package edu.albany.wsg.mysql.utility;


public class CustomResponse implements java.io.Serializable{

	private static final long serialVersionUID = -7576044225250723284L;

	private ResponseStatus code;
	private String desc;
	private Object obj;
	

	public CustomResponse(ResponseStatus success, String string, Object obj) {
		this.code = success;
		this.desc = string;
		this.obj = obj;
	}
	
	public CustomResponse() {
		
	}
	public ResponseStatus getCode() {
		return code;
	}
	public void setCode(ResponseStatus success) {
		this.code = success;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Object getObj() {
		return obj;
	}
	public void setObj(Object obj) {
		this.obj = obj;
	}
	
}
