package edu.albany.wsg.mysql.service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import com.mysql.jdbc.Statement;

import edu.albany.wsg.mysql.utility.DatabaseUtils;

@Service("Service")
public class SqlService {
	final static Logger logger = Logger.getLogger(SqlService.class);
	@Autowired
	public DatabaseUtils db;
		

	public String get(String query){
		System.out.println("[GET] Query : " +query);
		
		List<Map<String, Object>> rows = db.getJdbcTemplate().queryForList(query);
		String log = "insert into  (";
		if(!rows.isEmpty()){
			try {
				return convertTOJson(rows);
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public List<Map<String, Object>> update( String query){
		final String q2 =query;
		List<Map<String, Object>> output = new ArrayList<Map<String,Object>>();
		if(!query.toLowerCase().startsWith("insert")){
			System.out.println("[POST] Query : " +query);
			Map<String,Object> map = new HashMap<String,Object>();
			
			output.add((Map<String, Object>) map.put("key", db.getJdbcTemplate().update(query)));
			return output;
		}
		else{
			GeneratedKeyHolder holder = new GeneratedKeyHolder();
			db.getJdbcTemplate().update(
					new PreparedStatementCreator() {
				        public PreparedStatement createPreparedStatement(Connection con)
				                throws SQLException {
				        	 return con.prepareStatement(q2, Statement.RETURN_GENERATED_KEYS);
				        }
				    }, holder
				);
			return holder.getKeyList();
		}
	}

	private String convertTOJson(Object obj) throws JsonGenerationException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(obj);
	}
	
	
}
