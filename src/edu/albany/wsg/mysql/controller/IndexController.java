package edu.albany.wsg.mysql.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import edu.albany.wsg.mysql.service.SqlService;
import edu.albany.wsg.mysql.utility.CustomResponse;
import edu.albany.wsg.mysql.utility.DatabaseUtils;
import edu.albany.wsg.mysql.utility.ResponseStatus;
import edu.albany.wsg.mysql.utility.ResponseUtil;
import net.sf.json.JSONObject;


@RestController
@RequestMapping("/mysql/api/v1/")
public class IndexController {
	
	final static Logger logger = Logger.getLogger(IndexController.class);
	
	@Autowired
	public DatabaseUtils db;
	
	@Autowired
	public ResponseUtil responseUtil;
	
	@Resource
	private SqlService Service;

	@CrossOrigin
	@RequestMapping(value = "/POST", method = RequestMethod.POST)
	public  CustomResponse POST(HttpServletRequest request,  HttpServletResponse response)  {
		try{
			String dbstr = request.getAttribute("database").toString();
			String q = request.getAttribute("query").toString();
			if(dbstr.isEmpty()){
				CustomResponse cr = responseUtil.fail();
				cr.setDesc("Should provide database");
				return cr;
			}
			db.setURL(dbstr);
			if(q.isEmpty()){
				CustomResponse cr = responseUtil.fail();
				cr.setDesc("No query be found");
				return cr;
				}
			List<Map<String, Object>> flag =Service.update(q);
			
			if(flag.isEmpty()){
				CustomResponse cr = responseUtil.fail();
				cr.setDesc("Operation fails:"
						+ "[database]:"+dbstr
						+"\n[query]:"+q);
				return cr;
			}else{
				CustomResponse cr = new CustomResponse();
				cr.setCode(ResponseStatus.success);
				cr.setDesc("Record have been updated.[id]="+flag);
				cr.setObj(flag);
				return cr;
			}
		}catch(Exception e){
				CustomResponse cr = responseUtil.fail();
				cr.setDesc("Operation fails :"
						+ "[Error] :"+e
						);
				return cr;
		}
		
		
	}
	
	@CrossOrigin
	@RequestMapping(value = "/get",method = RequestMethod.POST)
	public  CustomResponse GET( HttpServletRequest request,  HttpServletResponse response) throws IOException {
		String dbstr = request.getAttribute("database").toString();
		if(dbstr.isEmpty()){
			CustomResponse cr = responseUtil.fail();
			cr.setDesc("Should provide database");
			return cr;
		}
		db.setURL(dbstr);
		String q = request.getAttribute("query").toString();
		if(q.isEmpty()){
			CustomResponse cr = responseUtil.fail();
			cr.setDesc("No query be found");
			return cr;
			}
		String result=Service.get(q);
		if(null==result){
			CustomResponse cr = responseUtil.fail();
			cr.setDesc("No data aviliable. ");
			return cr;
		}else{
			CustomResponse cr = new CustomResponse();
			cr.setCode(ResponseStatus.success);
			cr.setDesc("Operation Success");
			cr.setObj(result);
			return cr;
		}
		
	}
	
	@CrossOrigin
	@RequestMapping(value = "/{dbname}/{query}", method = RequestMethod.GET)
	public  CustomResponse simpleGET(HttpServletRequest request,@PathVariable String dbname, @PathVariable String query) throws IOException {
		System.out.print("GET");
		db.setURL(dbname);
		String result=Service.get(query);
		if(null==result||result.equals(null)){
			CustomResponse cr = responseUtil.fail();
			cr.setDesc("No data aviliable."
					+ "[database]:"+dbname
					+"\n[query]:"+query);
			return cr;
		}else{
			CustomResponse cr = new CustomResponse();
			cr.setCode(ResponseStatus.success);
			cr.setDesc("Operation Success");
			cr.setObj(result);
			return cr;
		}
	}
	
	
	
	@CrossOrigin
	@RequestMapping(value = "/urlNotMap", method = RequestMethod.GET)
	public  CustomResponse UnkownURLGet(HttpServletRequest request) throws IOException {
			CustomResponse cr = responseUtil.fail();
			cr.setDesc("URL Does Not Map");
			return cr;
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/urlNotMap", method = RequestMethod.POST)
	public  CustomResponse UnkownURLPOST(HttpServletRequest request) throws IOException {
			CustomResponse cr = responseUtil.fail();
			cr.setDesc("URL Does Not Map");
			return cr;
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/services/external/Post", method = RequestMethod.POST)
	public  CustomResponse externalPost(HttpServletRequest request) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader reader = request.getReader();
		try {
			String line;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} finally {
			reader.close();
		}
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		JsonElement jsonTree = parser.parse(sb.toString());
		 Form form =new Form();
		if (jsonTree.isJsonObject()) {
			JsonObject jObj = jsonTree.getAsJsonObject();
			Set<Map.Entry<String, JsonElement>> entries = jObj.entrySet();
			for (Map.Entry<String, JsonElement> entry : entries) {
				    form.param(entry.getKey(), entry.getValue().toString().substring(1,
						entry.getValue().toString().length() - 1));

			}
			return Post(form,request.getAttribute("domain").toString(),request.getAttribute("path").toString());
		}
		CustomResponse cr = responseUtil.fail();
		cr.setDesc("Call Service fails");
		return cr;
	}
	
	public CustomResponse Post(Form form,String domain,String path1){
		ClientConfig config = new ClientConfig();
	    Client client = (Client) ClientBuilder.newClient(config);
	    
        URI uri=UriBuilder.fromUri(domain).build();
	    WebTarget target = client.target(uri);
	    
	 
	    // get response 
	    Response response2 = target.path(path1).request().post(Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED),Response.class);
	    JSONObject obj=JSONObject.fromObject(response2.getStatus());
	    if(response2.getStatus()!=200){
	    	// login fails
	    	CustomResponse cr = responseUtil.fail();
			cr.setDesc("Call Service fails");
			return cr;
	    	
	    }else{
	    	// login success
		    String obj2=target.path("local").request().post(Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED),String.class);
		    CustomResponse cr = new CustomResponse();
			cr.setCode(ResponseStatus.success);
			cr.setDesc("login Success");
			cr.setObj(obj2);
			return cr;
	    }
	}
	
	@CrossOrigin
	@RequestMapping(value = "/services/external/Get", method = RequestMethod.GET)
	public  CustomResponse externalGet(HttpServletRequest request) throws IOException {
			CustomResponse cr = new CustomResponse();
			cr.setCode(ResponseStatus.success);
			cr.setObj(request.getAttribute("path").toString());
			return cr;
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/invaild", method = RequestMethod.GET)
	public  CustomResponse invaildGet(HttpServletRequest request) throws IOException {
			CustomResponse cr = responseUtil.fail();
			cr.setDesc( request.getAttribute("message").toString());
			return cr;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/invaild", method = RequestMethod.POST)
	public  CustomResponse invaildPOST(HttpServletRequest request) throws IOException {
			CustomResponse cr = responseUtil.fail();
			cr.setDesc( request.getAttribute("message").toString());
			return cr;
	}
	
	
	

}


