package edu.albany.wsg.filter;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
/**
 * Servlet Filter implementation class URLFilter
 */

public class URLFilter implements Filter {
	
	
	
	static Logger log = Logger.getLogger(URLFilter.class);

	private DataSource dataSource;

	private JdbcTemplate jdbcTemplate;
	
	public URLFilter(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// Get Service Mapper from database.
		UUID idOne = UUID.randomUUID();
		log.info("[Info][" + idOne + "] User enter services ");
		
		String ipAddress =  request.getRemoteAddr();
		
		HttpServletRequest req = (HttpServletRequest) request;
		String requestURI = req.getRequestURI();
		
		// flag if find the pattern
		boolean flag = false;
		if (req.getMethod() == "GET") {  // if request is GET method.
			log.info("[Info] [" + idOne + "] Require a Get Service");
			if (requestURI.equals("/WSG/") || requestURI.startsWith("/WSG/resource/")) { 	// if reques is for resource , then do nothing g .
				flag = true;
				chain.doFilter(request, response);
				log.info("[Info] [" + idOne + "] Require a resource");
			} else {
				// Get all the service with active status
				List<Map<String, Object>> rows = jdbcTemplate
						.queryForList("select * from services where method='GET' and status='Active'  and isdeleted=0 ");
				HashMap hash = new HashMap();
				String[] pattern;
				String[] url;
				
				for (Map<String, Object> row : rows) { // compare each path in db with input 
				
					pattern = row.get("requested_router").toString().split("/");
					url = requestURI.split("/");
					
					// Store User token;
					String token = "" ;
					//  Get parameter list
					
					String database="itm3";
					
					List<String> parms = new ArrayList<String>();
					
					if (pattern.length + 1 == url.length) {// Check Path's length
						int i;
						for (i = 1; i < pattern.length; i++) {
							if (pattern[i].equals(url[i + 1])){  // Check item in path
								continue;
							} else {
								if (pattern[i].startsWith("{")) {// if item in path not the same , but it is parameter past from client side.r
									if (pattern[i].equals("{token}")) {// check if it is token
										token = url[i + 1];
									}
									if (pattern[i].equals("{database}")) {// check if it is database
										database = url[i + 1];
									}
									parms.add(url[i + 1]);
									continue;
								}
								break;
							}
						}// end for loop 
					
						if (i == pattern.length){ 
							// Check User info
							// if user's token is invaild then return invaild
							flag = true;
							if (!requestURI.equals("/login/")){    // if not login function
							if (!CheckToken(token)) { // if invaild token
								token = "";
								request.setAttribute("message", "Invaild User, Please using correct token");
								inVaildUser(request, response);
								log.info("[Info] [" + idOne + "] Invaild token");;
								recordActions(ipAddress,token,"", "", "Invaild User, Please using correct token", row.get("requested_router").toString());
								break;
							}else{
								if(!CheckPermission((int)row.get("id"),token,database)){  // if invaild Permission
									request.setAttribute("message", "Permission denial.");
									inVaildUser(request, response);
									log.info("[Info] [" + idOne + "] Permission denial.");
									recordActions(ipAddress,token,"", "", "Permission denial.", row.get("requested_router").toString());
									break;
								}
							}
							
						}
							
							
					   }
					}
					if (flag) { // if find the path , token is vaild and has permission
						if (token == "")
							break;
						
						if (row.get("type").toString().toLowerCase().equals("external")) { // Determinate if it is internal or external services
							log.info("[Info] [" + idOne + "] Request an external service.");
								int j = 0;
							String query = row.get("path").toString();
							Pattern pattern2 = Pattern.compile("@(\\w*)");
							Matcher matcher = pattern2.matcher(query);
							while (matcher.find()) {
								String e = matcher.group(0);
								query = query.replaceAll(matcher.group(0), parms.get(j));
								j++;
							}
							request.setAttribute("domain", row.get("domain").toString());
							request.setAttribute("path", query);
							doExternalService(request, response, "GET");
							break;
						} // end with external  service
						;

						// get internal service router which determinate with
						// Servlets will be used

						String[] iURL = row.get("internal_service_router").toString().split("/");

						int j = 1;
						if (iURL[1].startsWith("{")) {
							iURL[1] = parms.get(j);
							j++;
						}
						// construct the query
						String query = row.get("sqlquery").toString();
						Pattern pattern2 = Pattern.compile("@(\\w*)");
						Matcher matcher = pattern2.matcher(query);
						while (matcher.find()) {
							String e = matcher.group(0);
							query = query.replaceAll(matcher.group(0), parms.get(j));
							j++;

						}
						log.info("[Info] [" + idOne + "] Internal service : "+query);

						String url2 = "/mysql/api/v1/" + iURL[1] + "/" + query;
						recordActions(ipAddress,token,"", query, "Get find resource", row.get("requested_router").toString());
						request.getRequestDispatcher(url2).forward(request, response);
						break;
					}
				
				}
				
			}
		
		
		}else{
			log.info("[Info] [" + idOne + "] Require a POST Service");
			// Get All Service
			List<Map<String, Object>> rows = jdbcTemplate
					.queryForList("select * from services where method='post' and status='Active' and isdeleted=0");
			
			HashMap hash = new HashMap();
			String[] pattern;
			String[] url=requestURI.split("/");
			
			for (Map<String, Object> row : rows) {
				// find path in database
				List<String> parms = new ArrayList<String>();
				pattern = row.get("requested_router").toString().split("/");
				if (pattern.length + 1 == url.length) {
					int i;
					for (i = 1; i < pattern.length; i++) {
						if (pattern[i].equals(url[i + 1])) {
							continue;
						} else {
							if (pattern[i].startsWith("{")) {
								parms.add(url[i + 1]);
								continue;
							}
							break;
						}
					}
					if (i == pattern.length) // reach end , find path in db
						flag = true;
				}
			
					
				boolean isInvaildUser = false;
				
				if (flag) {
					String token="";
					String query ="";
					String database="";
					
					if (row.get("type").toString().toLowerCase().equals("external")) {
						query = row.get("path").toString();
					}else{
						query = row.get("sqlquery").toString();
					}
					
					StringBuilder sb = new StringBuilder();
					BufferedReader reader = req.getReader();
					try {
						String line;
						while ((line = reader.readLine()) != null) {
							sb.append(line);
						}
					} finally {
						reader.close();
					}
					
					// Builder SQL
					request.setAttribute("database", "");
					Gson gson = new Gson();
					JsonParser parser = new JsonParser();
					JsonElement jsonTree = parser.parse(sb.toString());
					if (jsonTree.isJsonObject()) {
						JsonObject jObj = jsonTree.getAsJsonObject();
						Set<Map.Entry<String, JsonElement>> entries = jObj.entrySet();
						for (Map.Entry<String, JsonElement> entry : entries) {
							if (entry.getKey().equals("token")) {
								//get token 
								token = entry.getValue().toString().substring(1,
										entry.getValue().toString().length() - 1);
								// exception
								if (!requestURI.endsWith("/login")&&!requestURI.endsWith("/token")&&!requestURI.endsWith("/user/add/local")&&!requestURI.endsWith("	/user/register")){
								
								// Check token
								if (!CheckToken(token)) 
								{
									isInvaildUser=true;
									token = "";
									request.setAttribute("message", "Invaild User, Please using correct token");
									inVaildUser(request, response);
									recordActions(ipAddress,token,"", "", "Invaild User, Please using correct token", row.get("requested_router").toString());
									break;
								}
						         }else{
						        	 	isInvaildUser=false;
										token = "login/token";
						         }
						       }
							
							if (entry.getKey().equals("database")){
								database=entry.getValue().toString().substring(1,
										entry.getValue().toString().length() - 1);
								request.setAttribute("database", entry.getValue().toString().substring(1,
										entry.getValue().toString().length() - 1));
							}
								
							if (entry.getValue().getAsJsonPrimitive().isNumber()) {
								query = query.replaceAll("@" + entry.getKey(), entry.getValue().toString());
							} else {
								query = query.replaceAll("@" + entry.getKey(), entry.getValue().toString().substring(1,
										entry.getValue().toString().length() - 1));
							}

						}
					}
					if (!requestURI.endsWith("/login")&&!requestURI.endsWith("/token")&&!requestURI.endsWith("/user/add/local")&&!requestURI.endsWith("	/user/register")){
						// Check Permission
						if(token!=""&&database!=""){
							if(!CheckPermission((int)row.get("id"),token,database)){ 
								isInvaildUser=true;
								request.setAttribute("message", "Permission denial.");
								inVaildUser(request, response);
								recordActions(ipAddress,token,"", "", "Permission denial.", row.get("requested_router").toString());
								break;
							
						        }
						}else{
							request.setAttribute("message", "Token and database information are not provided.");
							recordActions(ipAddress,token,"", "", "Token and database information are not provided.", row.get("requested_router").toString());
							inVaildUser(request, response);
							break;
						}
						}
					
					// Forward to controller
					if (!isInvaildUser&&token!=""||requestURI.endsWith("/login")||requestURI.endsWith("/token")) {
						
						if (row.get("type").toString().toLowerCase().equals("external")) {
							// to deal with external service
							request.setAttribute("domain", row.get("domain").toString());
							request.setAttribute("path",query);
							doExternalService(request, response, "POST");
							break;	
						};
						
						request.setAttribute("query", query);
						String url2 = "/mysql/api/v1" + row.get("internal_service_router").toString();
						System.out.println("POST : " + url2);
						recordActions(ipAddress,token,request.getAttribute("database").toString(), query, "POST Action", row.get("requested_router").toString());
						request.getRequestDispatcher(url2).forward(request, response);
						break;
					} else {
						log.warn("[warn] invaild user ");
						break;
					}
				
				}
			}
			// if URL Not Map
			if (!flag) {
				log.warn("[warn][" + idOne + "]   url is not mapped  ");
				String url2 = "/mysql/api/v1/urlNotMap";
				recordActions(ipAddress,"","","", "url is not mapped ", requestURI);
				request.getRequestDispatcher(url2).forward(request, response);
			
			}
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	
	

	private void doExternalService(ServletRequest request, ServletResponse response, String type) {

		if (type.equals("GET")) {
			String url2 = "/mysql/api/v1/services/external/Get";
			
			try {
				request.getRequestDispatcher(url2).forward(request, response);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		if (type.equals("POST")) {
			String url2 = "/mysql/api/v1/services/external/Post";
			try {
				request.getRequestDispatcher(url2).forward(request, response);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	private void inVaildUser(ServletRequest request, ServletResponse response) {

		log.warn("[warn] invaild user ");
		String url2 = "/mysql/api/v1/invaild";
		try {
			request.getRequestDispatcher(url2).forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	private boolean CheckToken(String token) {
		String sql = "SELECT token FROM user WHERE token = ?";

		 List<String> rtoken =  jdbcTemplate.queryForList(sql, new Object[] { token }, String.class);
		if ( rtoken.isEmpty() )
			  return false;
		return true;
	}

	
	
	private boolean CheckPermission(int serviceId, String token, String db) {
		String sql = "";
		Integer typeId =-1;
		if(!db.toLowerCase().trim().equals("itm3")){ 
			sql ="SELECT type FROM "+db+".user WHERE token =?";
			typeId=(Integer) jdbcTemplate.queryForObject(sql, new Object[] { token }, Integer.class);
		}else{
			// user has its role in each community , however, the role database itm3 will be same as admin
			typeId=2;
		}
		String sql2 = "SELECT service_id FROM services_permission WHERE type_id ="+typeId;
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql2);
		boolean isvalid = false;
		for (Map row : rows) {
			if ((Integer) row.get("service_id") == serviceId) {
				isvalid = true;
				break;
			};
		}
		return isvalid;
	}
	
	
	private void recordActions(String ip,String token, String database, String query,String messge,String path) {
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
		query=query.replaceAll("'","\"" );
		String sql="insert into user_query_record(ip,dated,user_token,path,user_query,message,db) value('"+ip+"','"+timeStamp+"','"+token+"','"+path+"','"+query+"','"+messge+"','"+database+"')";
		System.out.println(sql);
		jdbcTemplate.execute(sql);
	}

}
